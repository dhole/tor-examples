package main

import (
	"context"
	"fmt"
	"github.com/cretz/bine/control"
	//"github.com/cretz/bine/tor"
	"log"
	"net/textproto"
	"os"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %v ONION_ID\n", os.Args[0])
		return
	}
	address := os.Args[1]

	log.Println("Connecting to Tor control port...")
	conn, err := textproto.Dial("tcp", "127.0.0.1:9051")
	if err != nil {
		log.Panicf("Unable to connect to Tor control port: %v\n", err)
	}
	cont := control.NewConn(conn)
	cont.DebugWriter = os.Stdout
	if err := cont.Authenticate("oeVKCmeqaeUhEJmD"); err != nil {
		log.Panicf("Error authenticating with Tor control port: %v\n", err)
	}
	log.Println("Connected and authenticated to Tor control port!")

	//log.Println("Starting Tor process...")
	//t, err := tor.Start(nil, nil)
	//if err != nil {
	//	log.Panicf("Unable to start Tor: %v\n", err)
	//}
	//defer t.Close()
	//readyCtx, readyCancel := context.WithTimeout(context.Background(), 3*time.Minute)
	//defer readyCancel()
	//log.Println("Connecting to the Tor network...")
	//if err := t.EnableNetwork(readyCtx, true); err != nil {
	//	log.Panicf("Unable to connect to the Tor network: %v\n", err)
	//}
	//log.Println("Connected to the Tor network!")

	log.Printf("Requesting onion service descriptor for %v ...\n", address)
	if err := cont.GetHiddenServiceDescriptorAsync(address, ""); err != nil {
		log.Panicf("Error requesting onion service descriptor for %v: %v\n", address, err)
	}

	eventWaitCtx, eventWaitCancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer eventWaitCancel()
	event, err := cont.EventWait(eventWaitCtx, []control.EventCode{control.EventCodeHSDesc, control.EventCodeHSDescContent},
		func(event control.Event) (bool, error) {
			if event.Code() == control.EventCodeHSDescContent {
				return true, nil
			} else {
				return false, nil
			}
		})
	if err != nil {
		log.Panicf("Error reading onion service descriptor for %v: %v\n", address, err)
	}
	log.Printf("\tevent:\n%+v\n", event)
}
