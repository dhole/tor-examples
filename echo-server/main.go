package main

import (
	"bufio"
	"context"
	"fmt"
	"github.com/cretz/bine/tor"
	"log"
	"net"
	"time"
)

func acceptConnLoop(onion *tor.OnionService, msgCh chan string, errCh chan error) {
	n := 1
	for {
		conn, err := onion.Accept()
		if err != nil {
			errCh <- err
			return
		}
		msgCh <- fmt.Sprintf("[I] Accepted new onion connection num. %v", n)
		go printMsgLoop(conn, n, msgCh)
		n++
	}
}

func printMsgLoop(conn net.Conn, n int, msgCh chan string) {
	defer conn.Close()
	bufConRead := bufio.NewReader(conn)
	for {
		msg, err := bufConRead.ReadBytes('\n')
		if err != nil {
			msgCh <- fmt.Sprintf("[E] Error reading from onion connection: %v", err)
			return
		}
		msgCh <- fmt.Sprintf("[M] %v. %v", n, string(msg[:len(msg)-1]))
	}
}

func main() {
	log.Println("Starting Tor process...")
	t, err := tor.Start(nil, nil)
	if err != nil {
		log.Panicf("Unable to start Tor: %v\n", err)
	}
	defer t.Close()
	readyCtx, readyCancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer readyCancel()
	log.Println("Connecting to the Tor network...")
	if err := t.EnableNetwork(readyCtx, true); err != nil {
		log.Panicf("Unable to connect to the Tor network: %v\n", err)
	}

	// Wait at most a few minutes to publish the service
	listenCtx, listenCancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer listenCancel()
	log.Println("Creating and registering onion service...")
	// Create an onion service to listen on any port but show as 80
	onion, err := t.Listen(listenCtx, &tor.ListenConf{RemotePorts: []int{80}})
	if err != nil {
		log.Panicf("Unable to create onion service: %v\n", err)
	}
	defer onion.Close()
	log.Printf("Onion service ready and listening on %v.onion:%v\n", onion.ID, onion.RemotePorts[0])

	errCh := make(chan error, 1)
	msgCh := make(chan string)
	go func() {
		for {
			msg := <-msgCh
			log.Println(msg)
		}
	}()
	go acceptConnLoop(onion, msgCh, errCh)

	log.Println("Press enter to exit")
	go func() {
		fmt.Scanln()
		errCh <- nil
	}()

	if err = <-errCh; err != nil {
		log.Panicf("Error accepting onion connection: %v\n", err)
	}
}
