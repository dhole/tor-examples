package main

import (
	"bufio"
	//"context"
	"fmt"
	//"github.com/cretz/bine/tor"
	"log"
	"os"
	//"time"

	"golang.org/x/net/proxy"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %v ONION_ADDR\n", os.Args[0])
		return
	}
	address := os.Args[1]

	//log.Println("Starting Tor process...")
	//t, err := tor.Start(nil, nil)
	//if err != nil {
	//	log.Panicf("Unable to start Tor: %v\n", err)
	//}
	//defer t.Close()
	//readyCtx, readyCancel := context.WithTimeout(context.Background(), 3*time.Minute)
	//defer readyCancel()
	//log.Println("Connecting to the Tor network...")
	//if err := t.EnableNetwork(readyCtx, true); err != nil {
	//	log.Panicf("Unable to connect to the Tor network: %v\n", err)
	//}

	//dialCtx, dialCancel := context.WithTimeout(context.Background(), 1*time.Minute)
	//defer dialCancel()
	//torDialer, err := t.Dialer(dialCtx, nil)
	//if err != nil {
	//	log.Panicf("Unable to connect to the Tor network: %v\n", err)
	//}
	//log.Println("Connected to the Tor network!")

	////

	torDialer, err := proxy.SOCKS5("tcp", "127.0.0.1:9050", nil, nil)
	if err != nil {
		log.Panicf("Unable to connect to the Tor daemon: %v\n", err)
	}
	log.Println("Connected to the Tor network!")

	////

	log.Printf("Connecting to onion service %v ...\n", address)
	conn, err := torDialer.Dial("tcp", address)
	if err != nil {
		log.Panicf("Error connecting to %v: %v\n", address, err)
	}
	log.Println("Connected to onion service!")
	defer conn.Close()
	bufConWrite := bufio.NewWriter(conn)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("> ")
	for scanner.Scan() {
		msg := scanner.Text()
		if _, err := bufConWrite.Write([]byte(fmt.Sprintf("%v\n", msg))); err != nil {
			log.Panicf("Error writing to %v: %v\n", address, err)
		}
		bufConWrite.Flush()
		fmt.Printf("> ")
	}
}
